package com.company;

public class Bottiglia {

    private String codice;
    private double prezzo;
    private Tipi tipo;
    private int anno;

    public  Bottiglia(String c, double p, String t, int a){
        setCodice(c);
        setPrezzo(p);
        setTipo(t);
        setAnno(a);
    }

    public boolean setCodice(String c){
        if(c!=null) {
            codice=c;
            return true;
        }
        else return false;
    }

    public boolean setPrezzo(double p){
        if(p>=0){
            prezzo=p;
            return true;
        }
        else return false;
    }

    public boolean setTipo(String t){
        if(Tipi.valueOf(t)!=null){
            Tipi.valueOf(t);
            return true;
        }
        else return false;
    }

    public boolean setAnno(int a){
        if(a>=0){
            anno=a;
            return true;
        }
        else return false;
    }

    public String getCodice(){
        return codice;
    }

    public double getPrezzo(){
        return prezzo;
    }

    public String getTipo(){
        return tipo.name();
    }

    public int getAnno(){
        return anno;
    }

    public Integer getAnnoByCodice(String code){
        if(codice.equalsIgnoreCase(code)){
            return anno;
        }
        else return null;
    }
}
