package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.server.ExportException;
import java.util.ArrayList;

public class GestioneBottiglie{

    private ArrayList<Bottiglia> bottiglie;

    public GestioneBottiglie(){
        bottiglie=new ArrayList<Bottiglia>();
    }


    public boolean addBottiglia(){
        BufferedReader keyboard= new BufferedReader(new InputStreamReader(System.in));
        String c=null;
        double p=0.0;
        String t=null;
        int a=0;
        try {
            System.out.println("Inserisci Codice");
            c=keyboard.readLine();
        } catch (Exception e){
            System.out.println("Errore nell'inserimento del codice\n  Codice errore -1");
            System.exit(-1);
        }

        try {
            System.out.println("Inserisci prezzo");
            p=Double.parseDouble(keyboard.readLine());
        } catch (Exception e){
            System.out.println("Errore nell'inserimento del prezzo\n Codice errore -2");
            System.exit(-2);
        }

        try {
            System.out.println("Inserisci tipo");
            t = keyboard.readLine();
        } catch (Exception e){
            System.out.println("Errore nell'inserimento del tipo\n Codice errore -3");
            System.exit(-3);
        }

        try {
            System.out.println("Inserisci l'anno di imbottigliamento");
            a=Integer.parseInt(keyboard.readLine());
        } catch (Exception e){
            System.out.println("Errore nell'inserimento dell'anno");
            System.exit(-4);
        }

       try {
           bottiglie.add(new Bottiglia(c, p, t, a));
           return true;
       } catch (Exception e){
            return false;
       }
    }

    public boolean removeBottiglia() {
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        String code = null;

        try {
            System.out.println("Inserisci il codice della bottiglia da rimuovere");
            code = keyboard.readLine();
        } catch (Exception e) {
            System.exit(-7);
        }

        Bottiglia b = bottiglie.get(Integer.parseInt(code));

        return bottiglie.remove(b);
    }


}
